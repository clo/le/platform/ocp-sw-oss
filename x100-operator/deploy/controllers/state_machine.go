/*
Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
SPDX-License-Identifier: BSD-3-Clause-Clear

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package controllers

import (
	"context"
	"fmt"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	corev1 "k8s.io/api/core/v1"
	apiextv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	xcardv1 "x100-operator/api/v1"
)

// State Machine
func (c *ControllerState) start(reconciler *X100ManagementPolicyReconciler, policy *xcardv1.X100ManagementPolicy, policySpec *xcardv1.X100ManagementPolicySpec) error {

	log.Log.Info("c *ControllerState start() Entered() for", " CR: ", policy.ObjectMeta.Name)
	// Initializate the ControllerState
	c.currentState = startState
	c.currentAsset = ""
	c.assets = assets
	c.x100Policy = policy
	c.rec = reconciler
	c.x100crd = &apiextv1.CustomResourceDefinition{}

	if err := c.rec.Get(context.TODO(), types.NamespacedName{Name: x100crdName}, c.x100crd); err != nil {
		log.Log.Info("Couldn't get x100 CRD", "Error", err)
		return err
	}

	// Initialize the AssetMap
	AssetMap = map[string]Asset{}

	// Start NFD services
	err := c.createNFDResources()
	if err != nil {
		return err
	}

	//fetch all nodes and label x100 nodes
	err = c.labelX100Nodes(policy, policySpec)
	if err != nil {
		return err
	}

	err = c.labelX100NodeswithCR(policy, policySpec)
	if err != nil {
		return err
	}
	// Transition to the Iterate Assets state
	c.desiredState = iterateAssetsState
	return nil
}

// Iterate over the assets
func (c *ControllerState) iterateAssets() (int, error) {
	assetLength := len(c.assets)
	// Check if there are any more assets to iterate over
	if assetLength == 0 {
		// No assets left, transition to end state
		//return endState, nil
		return getBootStatusState, nil
	} else {
		// More assets, set the current asset and transition to Create Resources
		c.currentAsset = c.assets[0]
		// Trim assets to exclude the assets before current asset
		c.assets = c.assets[1:]
		return createResourcesState, nil
	}
}

func (c *ControllerState) stateMachineCompleted() bool {
	if len(c.assets) == 0 {
		return true
	}
	return false
}

// Create resources for the current asset
func (c *ControllerState) createResources() (int, xcardv1.State, error) {
	// Create the resources for the current asset
	// Errors in this part should be handled inside createAssetMap by early exit
	_ = createAssetMap(c.currentAsset)

	// Create the resources using  resources from AssetMap and creation callbacks
	asset := AssetMap[c.currentAsset]

	result := xcardv1.Operational
	for _, robj := range asset.objectMappings {
		cardstate, err := createKindResource(*c, robj.key, robj.value)
		if err != nil {
			if errors.IsAlreadyExists(err) {
				if cardstate != xcardv1.Operational {
					result = xcardv1.NotOperational
				}
				continue
			}
			return earlyExitState, cardstate, err
		}
		if cardstate != xcardv1.Operational {
			result = xcardv1.NotOperational
		}
	}

	// Transition to the iterate assets state
	return iterateAssetsState, result, nil
}

func (c *ControllerState) getx100bootupStatus() (xcardv1.State, error) {
	/***
		Process only eligible nodes in labelx100bootupStatusforNodes
		Same goes for markHealthCheckedforCurrentCR and endState
	***/
	result := c.labelx100bootupStatusforNodes()
	c.markHealthCheckedforCurrentCR()
	return result, nil
}

func (c *ControllerState) endState() (xcardv1.State, error) {
	// Terminate
	/***
		Make an additional check to ensure that all nodes
		under current policy have actually been marked with boot status
		if not return NotOperational
		else Operational
	***/
	result := xcardv1.Operational
	opts := []client.ListOption{&client.MatchingLabels{x100ActiveCR: c.x100Policy.ObjectMeta.Name}}

	log.Log.Info("DEBUG: endState()", "LabelSelector", fmt.Sprintf("%s=%s",
		x100ActiveCR, c.x100Policy.ObjectMeta.Name))

	list := &corev1.NodeList{}
	err := c.rec.List(context.TODO(), list, opts...)
	if err != nil {
		return xcardv1.NotOperational, fmt.Errorf("Unable to list nodes to check labels, err %s", err.Error())
	}

	for _, node := range list.Items {
		labels := c.getNodeLabels(node)
		if !hasX100BootupStatusMarkedLabel(labels) {
			// Atleast one node still doesn't have boot status marked
			result = xcardv1.NotOperational
			continue
		}
		log.Log.Info(fmt.Sprintf("Reached endstate for node %s", node.ObjectMeta.Name))
	}

	return result, nil
}

func (c *ControllerState) triggerStateMachine(reconciler *X100ManagementPolicyReconciler, policy *xcardv1.X100ManagementPolicy) (xcardv1.State, error) {
	var err error
	var desiredState int
	var xcardStatus xcardv1.State
	var cardState xcardv1.State

	for {
		switch c.desiredState {
		case iterateAssetsState:
			//log.Log.Info("ERROR: triggerStateMachine - iterateAssetsState")
			desiredState, err = c.iterateAssets()
			if err != nil {
				return xcardv1.NotOperational, err
			}
			c.desiredState = desiredState
		case createResourcesState:
			//log.Log.Info("ERROR: triggerStateMachine - createResourcesState")
			desiredState, cardState, err = c.createResources()
			if err != nil {
				return cardState, err
			}
			if cardState != xcardv1.Operational {
				return cardState, nil
			}
			c.desiredState = desiredState
		case getBootStatusState:
			//log.Log.Info("ERROR: triggerStateMachine - getBootStatusState")
			cardState, err = c.getx100bootupStatus()
			if cardState != xcardv1.Operational {
				return cardState, err
			}
			c.desiredState = endState
		case endState:
			//log.Log.Info("ERROR: triggerStateMachine - endState")
			xcardStatus, err = c.endState()
			if err != nil {
				return xcardv1.NotOperational, err
			} else {
				return xcardStatus, nil
			}
		case earlyExitState:
			//log.Log.Info("ERROR: triggerStateMachine - earlyExitState")
			xcardStatus, err = xcardv1.NotOperational, nil
			if err != nil {
				return xcardv1.NotOperational, err
			} else {
				return xcardStatus, nil
			}
		}
	}
}
