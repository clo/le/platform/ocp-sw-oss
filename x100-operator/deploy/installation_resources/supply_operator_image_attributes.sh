#!/bin/bash

#
#Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
#SPDX-License-Identifier: BSD-3-Clause-Clear
#
#


# Check if both parameters are provided
if [ "$#" -ne 2 ]; then
 echo "Usage: $0 <operator_image_url> <secret>"
 exit 1
fi

operator_image_url="$1"
secret="$2"

csv_path="./bundle/manifests"

for file in $(ls "$csv_path"/*"clusterserviceversion"*); do
	if [[ -f "$file" ]]; then
		echo "$file"
		# Replace placeholders with provided values
		sed -i "s|X100_OPERATOR_IMAGE_URL|$operator_image_url|g" "$file"
		sed -i "s|X100_OPERATOR_IMAGE_SECRET|$secret|g" "$file"
	fi
done

echo "X100 Operator Image : $operator_image_url"
echo "X100 Operator Image Secret : $secret"
