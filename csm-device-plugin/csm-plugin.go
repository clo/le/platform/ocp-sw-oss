/* Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
SPDX-License-Identifier: BSD-3-Clause-Clear
*/

// Copyright 2020 the generic-device-plugin authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
        "context"
        "fmt"
        "sync"
        "time"
        "strings"

        "github.com/go-kit/kit/log"
        "github.com/go-kit/kit/log/level"
        "github.com/prometheus/client_golang/prometheus"
        "k8s.io/kubelet/pkg/apis/deviceplugin/v1beta1"
)

const (
        deviceCheckInterval = 5 * time.Second
)

// DeviceSpec defines a device that should be discovered and scheduled.
// DeviceSpec allows multiple host devices to be selected and scheduled fungibly under the same name.
// Furthermore, host devices can be composed into groups of device nodes that should be scheduled
// as an atomic unit.
type DeviceSpec struct {
        // Name is a unique string representing the kind of device this specification describes.
        Name string `json:"name"`
        // Groups is a list of groups of devices that should be scheduled under the same name.
        Groups []*Group `json:"groups"`
}

// Default applies default values for all fields that can be left empty.
func (d *DeviceSpec) Default() {
        for _, g := range d.Groups {
                if g.Count == 0 {
                        g.Count = 1
                }
                for _, p := range g.Paths {
                        if p.Limit == 0 {
                                p.Limit = 1
                        }
                        if p.Type == "" {
                                p.Type = DevicePathType
                        }
                        if p.Type == DevicePathType && p.Permissions == "" {
                                p.Permissions = "mrw"
                        }
                }
        }
}

// Group represents a set of devices that should be grouped and mounted into a container together as one single meta-device.
type Group struct {
        // Paths is the list of devices of which the device group consists.
        // Paths can be globs, in which case each device matched by the path will be schedulable `Count` times.
        // When the paths have differing cardinalities, that is, the globs match different numbers of devices,
        // the cardinality of each path is capped at the lowest cardinality.
        Paths []*Path `json:"paths"`
        // Count specifies how many times this group can be mounted concurrently.
        // When unspecified, Count defaults to 1.
        Count uint `json:"count,omitempty"`
}

// device wraps the v1.beta1.Device type to add context about
// the device needed by the CsmPlugin.
type device struct {
        v1beta1.Device
        deviceSpecs []*v1beta1.DeviceSpec
        mounts      []*v1beta1.Mount
}

// CsmPlugin is a plugin for csm-* devices that can:
// * be found using a file path ; and
// * mounted and used without special logic.
type CsmPlugin struct {
        ds      *DeviceSpec
        devices map[string]device
        logger  log.Logger
        mu      sync.Mutex

        // metrics
        deviceGauge        prometheus.Gauge
        allocationsCounter prometheus.Counter
}

// NewCsmPlugin creates a new plugin for a csm- device.
func NewCsmPlugin(ds *DeviceSpec, pluginDir string, logger log.Logger, reg prometheus.Registerer, dummyvar bool) Plugin {
        if logger == nil {
                logger = log.NewNopLogger()
        }

        cp := &CsmPlugin{
                ds:      ds,
                devices: make(map[string]device),
                logger:  logger,
                deviceGauge: prometheus.NewGauge(prometheus.GaugeOpts{
                        Name: "csm_device_plugin_devices",
                        Help: "The number of devices managed by this device plugin.",
                }),
                allocationsCounter: prometheus.NewCounter(prometheus.CounterOpts{
                        Name: "csm_device_plugin_allocations_total",
                        Help: "The total number of device allocations made by this device plugin.",
                }),
        }

        if reg != nil {
                reg.MustRegister(cp.deviceGauge, cp.allocationsCounter)
        }

        return NewPlugin(ds.Name, pluginDir, cp, logger, prometheus.WrapRegistererWithPrefix("generic_", reg))
}

func (cp *CsmPlugin) discover() (devices []device, err error) {
        path, err := cp.discoverPath()
        if err != nil {
                return nil, fmt.Errorf("failed to discover path devices: %w", err)
        }

        return path, nil
}

// refreshDevices updates the devices available to the
// csm device plugin and returns a boolean indicating
// if everything is OK, i.e. if the devices are the same ones as before.
func (cp *CsmPlugin) refreshDevices() (bool, error) {
        devices, err := cp.discover()
        if err != nil {
                return false, fmt.Errorf("failed to refresh devices: %v", err)
        }

        cp.deviceGauge.Set(float64(len(devices)))

        cp.mu.Lock()
        defer cp.mu.Unlock()

        old := cp.devices
        cp.devices = make(map[string]device)

        var equal bool
        // Add the new devices to the map and check
        // if they were in the old map.
        for _, d := range devices {
                cp.devices[d.ID] = d
                if _, ok := old[d.ID]; !ok {
                        equal = false
                }
        }
        if !equal {
                return false, nil
        }

        // Check if devices were removed.
        for k := range old {
                if _, ok := cp.devices[k]; !ok {
                        return false, nil
                }
        }
        return true, nil
}

// GetDeviceState always returns healthy.
func (cp *CsmPlugin) GetDeviceState(_ string) string {
        return v1beta1.Healthy
}

// Allocate assigns generic devices to a Pod.
func (cp *CsmPlugin) Allocate(_ context.Context, req *v1beta1.AllocateRequest) (*v1beta1.AllocateResponse, error) {
        level.Info(cp.logger).Log("msg", fmt.Sprintf("ALLOCATE called()"))
        cp.mu.Lock()
        defer cp.mu.Unlock()
        res := &v1beta1.AllocateResponse{
                ContainerResponses: make([]*v1beta1.ContainerAllocateResponse, 0, len(req.ContainerRequests)),
        }
        for _, r := range req.ContainerRequests {
                resp := new(v1beta1.ContainerAllocateResponse)
                resp.Envs = make(map[string]string,len(r.DevicesIDs))
                var count_reqs int = 0
                var strs string = ""
                // Add all requested devices to to response.
                for _, id := range r.DevicesIDs {
                        d, ok := cp.devices[id]
                        if !ok {
                                return nil, fmt.Errorf("requested device does not exist %q", id)
                        }
                        if d.Health != v1beta1.Healthy {
                                return nil, fmt.Errorf("requested device is not healthy %q", id)
                        }
                        resp.Devices = append(resp.Devices, d.deviceSpecs...)
                        resp.Mounts = append(resp.Mounts, d.mounts...)

                        count_reqs++
                        if strings.Contains(resp.Devices[len(resp.Devices)-1].HostPath , "/dev/ptp") {
                           strs = fmt.Sprintf("MHIPHC_DEVICE_%d", count_reqs)
                           resp.Envs[strs] = resp.Devices[len(resp.Devices)-1].HostPath
                           level.Info(cp.logger).Log("msg", fmt.Sprintf("MHIPHC_DEVICE_%d----%s", count_reqs,resp.Envs[strs]))
                        } else {
                           strs = fmt.Sprintf("CSM_DEVICE_%d", count_reqs)
                           resp.Envs[strs] = resp.Devices[len(resp.Devices)-1].HostPath
                           level.Info(cp.logger).Log("msg", fmt.Sprintf("CSM_DEVICE_%d----%s", count_reqs,resp.Envs[strs]))
                        }
                }
                res.ContainerResponses = append(res.ContainerResponses, resp)
        }
        cp.allocationsCounter.Add(float64(len(res.ContainerResponses)))
        return res, nil
}

// GetDevicePluginOptions always returns an empty response.
func (cp *CsmPlugin) GetDevicePluginOptions(_ context.Context, _ *v1beta1.Empty) (*v1beta1.DevicePluginOptions, error) {
        return &v1beta1.DevicePluginOptions{}, nil
}

// ListAndWatch lists all devices and then refreshes every deviceCheckInterval.
func (cp *CsmPlugin) ListAndWatch(_ *v1beta1.Empty, stream v1beta1.DevicePlugin_ListAndWatchServer) error {
        level.Info(cp.logger).Log("msg", "starting listwatch")
        if _, err := cp.refreshDevices(); err != nil {
                return err
        }
        ok := false
        var err error
        for {
                if !ok {
                        res := new(v1beta1.ListAndWatchResponse)
                        for _, dev := range cp.devices {
                                res.Devices = append(res.Devices, &v1beta1.Device{ID: dev.ID, Health: dev.Health})
                        }
                        if err := stream.Send(res); err != nil {
                                return err
                        }
                }
                <-time.After(deviceCheckInterval)
                ok, err = cp.refreshDevices()
                if err != nil {
                        return err
                }
        }
}

// PreStartContainer always returns an empty response.
func (cp *CsmPlugin) PreStartContainer(_ context.Context, _ *v1beta1.PreStartContainerRequest) (*v1beta1.PreStartContainerResponse, error) {
        return &v1beta1.PreStartContainerResponse{}, nil
}

// GetPreferredAllocation always returns an empty response.
func (cp *CsmPlugin) GetPreferredAllocation(context.Context, *v1beta1.PreferredAllocationRequest) (*v1beta1.PreferredAllocationResponse, error) {
        return &v1beta1.PreferredAllocationResponse{}, nil
}
