/* Copyright (c) 2023 Qualcomm Innovation Center, Inc. All rights reserved.
SPDX-License-Identifier: BSD-3-Clause-Clear
*/

// Copyright 2020 the generic-device-plugin authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
        "fmt"
        "strings"
        "path/filepath"
        "io/ioutil"
        "os"

        "github.com/go-kit/kit/log"
        "github.com/go-kit/kit/log/level"
        "github.com/ghodss/yaml"
        "github.com/mitchellh/mapstructure"
        flag "github.com/spf13/pflag"
        "github.com/spf13/viper"
        "k8s.io/kubelet/pkg/apis/deviceplugin/v1beta1"
)

const defaultDomain = "qualcomm.com"

func find_mhiphcnode() string {
   pattern := "ptp*"
   var mhiphc_node string
   files, err := ioutil.ReadDir("/dev/")
   if err != nil {
      fmt.Println("Error:", err)
      panic(err)
   }
   var matchingFiles []string
   for _, file := range files {
      match, err := filepath.Match(pattern, file.Name())
      if err != nil {
         fmt.Println("Error:", err)
         panic(err)
      }
      if match {
         matchingFiles = append(matchingFiles, file.Name())
         cfname := fmt.Sprintf("/sys/class/ptp/%s/clock_name", file.Name())
         dat, err := os.ReadFile(cfname)
         if err != nil {
              fmt.Println("Error:", err)
              panic(err)
         }
         var mhi_clk string
         mhi_clk = "MHI_PHC"
         if strings.Compare(strings.TrimSpace(string(dat)), mhi_clk) == 0 {
            mhiphc_node = file.Name()
            fmt.Println("inside MHI_PHC", file.Name())
            break  // Currently advertises only on one PHC Node(first found one)
         }
      }
   }
   fmt.Println(" Matched ptp* Files: ", matchingFiles)
   fmt.Println("MHI-PTP node: ", string(mhiphc_node))
   return mhiphc_node
}

// initConfig defines config flags, config file, and envs
func initConfig() error {
        logger := log.NewJSONLogger(log.NewSyncWriter(os.Stdout))
        logger = level.NewFilter(logger, level.AllowAll())
        logger.Log("msg", "inside initConfig()")
        cfgFile := flag.String("config", "", "Path to the config file.")
        flag.String("domain", defaultDomain, "The domain to use when when declaring devices.")
        flag.StringArray("device", nil, `The devices to expose. This flag can be repeated to specify multiple device types.
Multiple paths can be given for each type. Paths can be globs.
Should be provided in the form:
{"name": "<name>", "groups": [(device definitions)], "count": <count>}]}
The device definition should be a path to a device file.
For device files, use something like: {"paths": [{"path": "<path-1>", "mountPath": "<mount-path-1>"},{"path": "<path-2>", "mountPath": "<mount-path-2>"}]}`)
        flag.String("plugin-directory", v1beta1.DevicePluginPath, "The directory in which to create plugin sockets.")
        flag.String("log-level", logLevelInfo, fmt.Sprintf("Log level to use. Possible values: %s", availableLogLevels))
        flag.String("listen", ":8080", "The address at which to listen for health and metrics.")
        flag.Bool("version", false, "Print version and exit")

        flag.Parse()
        if err := viper.BindPFlags(flag.CommandLine); err != nil {
                return fmt.Errorf("failed to bind config: %w", err)
        }
        logger.Log("msg", flag.CommandLine)
        if *cfgFile != "" {
                viper.SetConfigFile(*cfgFile)
        } else {
                viper.SetConfigName("config")
                viper.SetConfigType("yaml")
                viper.AddConfigPath("/etc/csm-device-plugin/")
                viper.AddConfigPath(".")
        }

        viper.AutomaticEnv()
        viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_", "-", "_"))

        if err := viper.ReadInConfig(); err != nil {
                if _, ok := err.(viper.ConfigFileNotFoundError); ok {
                        // Config file not found; ignore error
                } else {
                        // Config file was found but another error was produced
                        return fmt.Errorf("failed to read config file: %w", err)
                }
        }

        viper.RegisterAlias("devices", "device")

        return nil
}

// getConfiguredDevices returns a list of configured devices
func getConfiguredDevices() ([]*DeviceSpec, error) {
        switch raw := viper.Get("device").(type) {
        case []string:
                // Assign deviceSpecs from flag
                deviceSpecs := make([]*DeviceSpec, len(raw))
                for i, data := range raw {
                        if err := yaml.Unmarshal([]byte(data), &deviceSpecs[i]); err != nil {
                                return nil, fmt.Errorf("failed to parse device %q: %w", data, err)
                        }
                }
                return deviceSpecs, nil
        case []interface{}:
                // Assign deviceSpecs from config
                deviceSpecs := make([]*DeviceSpec, len(raw))
                for i, data := range raw {
                        decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
                                Result:  &deviceSpecs[i],
                                TagName: "json",
                                DecodeHook: nil,
                        })
                        if err != nil {
                                return nil, err
                        }

                        if err := decoder.Decode(data); err != nil {
                                return nil, fmt.Errorf("failed to decode device data %q: %w", data, err)
                        }
                }
                return deviceSpecs, nil
        default:
                return nil, fmt.Errorf("failed to decode devices: unexpected type: %T", raw)
        }
}
