# CSM Device Plugin

##Building:
1. make clean
2. make vendor
3. make
4. buildah build -f Dockerfile -t <image_name> .
5. podman login -u="<login>" -p="<login_token>" <docker-registry_URL>
6. podman push <imageID> <repository URL/image_name>
7. now, when deploying the x100 custom operator, edit the $SOURCE/config/samples/qualcomm_v1_x100managementpolicy.yaml in operator deployment phase and replace the csmDevicePlugin: block


## Usage
when deploying the DU Pods that require a csm device, they could request the csm devices using the Kubernetes Pod `resources` field of spec.containers.resources:
```yaml
resources:
  limits:
    qualcomm.com/qti-csm: 3
```
